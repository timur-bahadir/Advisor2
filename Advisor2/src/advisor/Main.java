package advisor;

import advisor.gui.GuiApp;

import javax.swing.*;

/**
 * Created by Timur on 06.04.2015.
 */
public class Main {
    public static void main(String[] args) {
        GuiApp guiApp = new GuiApp();

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        guiApp.start();
    }
}
