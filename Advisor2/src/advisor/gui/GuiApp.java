package advisor.gui;

import advisor.models.Nation;
import advisor.models.Vassal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by Timur on 10.04.2015.
 */
public class GuiApp implements PropertyChangeListener {
    private NumberFormat numberFormat;

    private JPanel rootPanel;
    private JTabbedPane tabbedPane;
    /*
    ==================================
    OverEx
    ==================================
     */
    private Nation nation;

    private JPanel overExPanel;

    private JSpinner admEffSpnr;
    private JSpinner.DefaultEditor editor;

    private JFormattedTextField unrestField;
    private JFormattedTextField overExField;

    private JTextField totalSafeTaxField;
    private JTextField uncTaxField;
    private JTextField safeTaxField;

    private JButton resetBtn;

    private JLabel admLabel;
    private JLabel unrestLabel;
    private JLabel tsbtLabel;
    private JLabel overexLabel;
    private JLabel ubtLabel;

    /*
    ==================================
    VASSALS
    ==================================
     */

    private ArrayList<Vassal> vassals;

    private MyTableModel model;

    private JPanel annexPanel;

    private JLabel sbtLabel;
    private JLabel devLabel;
    private JLabel nameLabel;
    private JLabel totalCostLabel;
    private JLabel monthlyProgLabel;

    private JFormattedTextField nameField;
    private JFormattedTextField totalCostField;
    private JFormattedTextField monthlyProgField;

    private JButton addButton;
    private JButton clearButton;

    private JTable vassalTable;

    public GuiApp() {
        nation = new Nation();
        vassals = new ArrayList<Vassal>();

        numberFormat = NumberFormat.getNumberInstance();
        editor = (JSpinner.DefaultEditor) admEffSpnr.getEditor();
        editor.getTextField().addPropertyChangeListener("value", this);

        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nation.reset();
                admEffSpnr.setValue(0);
                unrestField.setValue(0);
                overExField.setValue(0);
                updateOverExFields();
            }
        });


        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = (String) nameField.getValue();
                int totalCost = ((Number) totalCostField.getValue()).intValue();
                int monthlyProg = ((Number) monthlyProgField.getValue()).intValue();

                if (totalCost > 0 && monthlyProg > 0) {
                    clearVassalTable();

                    vassals.add(new Vassal(name, totalCost, monthlyProg));
                    Vassal.calculateStarts(vassals);

                    for (Vassal vassal : vassals) {
                        model.addRow(new Object[]{vassal.getName(), vassal.getTotalCost(),
                                vassal.getMonthlyProgress(), vassal.getMonthsNeeded(), vassal.getStartAt()});
                    }
                }

                nameField.setValue("");
                totalCostField.setValue(0.0d);
                monthlyProgField.setValue(0.0d);
            }
        });

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearVassalTable();
                vassals.clear();
            }
        });

        updateOverExFields();

        devLabel.setText("<html>Made by MelonHead. Website : <a href=\\\"\\\">https://timurbahadir.blogspot.com/</a></html>");
        devLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        devLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI("https://timurbahadir.blogspot.com"));
                } catch (URISyntaxException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private void clearVassalTable() {
        for (int i = 0; i < vassals.size(); i++) {
            model.removeRow(0);
        }
    }

    public boolean start() {
        JFrame frame = new JFrame("Advisor2");
        frame.setContentPane(new GuiApp().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
        return true;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object src = evt.getSource();
        if (src == unrestField || src == overExField || src == editor.getTextField()) {
            nation.reset();

            nation.setAdmEff((Integer) admEffSpnr.getValue());

            if (src == unrestField || src == editor.getTextField()) {
                nation.setUnrest((float) ((Number) unrestField.getValue()).doubleValue());
                nation.calcOverEx();
            }
            if (src == overExField) {
                nation.setOverEx((float) ((Number) overExField.getValue()).doubleValue());
                nation.calcUnrest();
            }

            nation.calcTaxes();
            updateOverExFields();
        }
    }

    private void updateOverExFields() {
        unrestField.setValue(nation.getUnrest());
        overExField.setValue(nation.getOverEx());
        totalSafeTaxField.setText(String.valueOf(nation.getTotalSafeTax()));
        uncTaxField.setText(String.valueOf(nation.getUncBaseTax()));
        safeTaxField.setText(String.valueOf(nation.getSafeBaseTax()));

        unrestField.setForeground((nation.getUnrest() > 5) ? Color.RED : Color.BLACK);
        overExField.setForeground((nation.getOverEx() > 100) ? Color.RED : Color.BLACK);
        safeTaxField.setForeground((nation.getSafeBaseTax() < 0) ? Color.RED : Color.BLACK);
    }

    private void createUIComponents() {
        createOverExComponents();
        createAnnexComponents();

        devLabel = new JLabel();
    }

    private void createOverExComponents() {
        admEffSpnr = new JSpinner(new SpinnerNumberModel(0, 0, 3, 1));

        unrestField = new JFormattedTextField(numberFormat);
        unrestField.setValue(0.0d);
        unrestField.setColumns(3);
        unrestField.addPropertyChangeListener("value", this);
        unrestField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        unrestField.selectAll();
                    }
                });
            }
        });

        overExField = new JFormattedTextField(numberFormat);
        overExField.setValue(0.0d);
        overExField.setColumns(3);
        overExField.addPropertyChangeListener("value", this);
        overExField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        overExField.selectAll();
                    }
                });
            }
        });
    }

    private void createAnnexComponents() {
        nameField = new JFormattedTextField();
        nameField.setValue("");
        nameField.setColumns(10);
        nameField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        nameField.selectAll();
                    }
                });
            }
        });

        totalCostField = new JFormattedTextField(numberFormat);
        totalCostField.setValue(0.0d);
        totalCostField.setColumns(3);
        totalCostField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        totalCostField.selectAll();
                    }
                });
            }
        });

        monthlyProgField = new JFormattedTextField(numberFormat);
        monthlyProgField.setValue(0.0d);
        monthlyProgField.setColumns(3);
        monthlyProgField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        monthlyProgField.selectAll();
                    }
                });
            }
        });

        model = new MyTableModel();
        vassalTable = new JTable(model);

        model.addColumn("Name");
        model.addColumn("Total Cost");
        model.addColumn("Monthly Progress");
        model.addColumn("Months");
        model.addColumn("Start At");
    }
}
