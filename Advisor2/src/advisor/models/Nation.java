package advisor.models;

/**
 * Created by Timur on 10.04.2015.
 */
public class Nation {

    public static final float UNREST_PER_OVEREX = 0.05f;

    private int admEff;
    private float unrest;
    private float overEx;
    private float totalSafeTax;
    private float uncBaseTax;
    private float safeBaseTax;

    public void calcUnrest() {
        unrest = overEx * UNREST_PER_OVEREX;
    }

    public void calcOverEx() {
        overEx = unrest / UNREST_PER_OVEREX;
    }

    public void calcTaxes() {
        totalSafeTax = (100.0f / (4 - admEff));
        uncBaseTax = (overEx / (4 - admEff));
        safeBaseTax = totalSafeTax - uncBaseTax;
    }

    public void reset() {
        admEff = 0;
        unrest = overEx = 0;

        calcTaxes();
    }

    public void setAdmEff(int admEff) {
        this.admEff = admEff;
    }

    public float getUnrest() {
        return unrest;
    }

    public void setUnrest(float unrest) {
        this.unrest = unrest;
    }

    public float getOverEx() {
        return overEx;
    }

    public void setOverEx(float overEx) {
        this.overEx = overEx;
    }

    public float getTotalSafeTax() {
        return totalSafeTax;
    }

    public float getUncBaseTax() {
        return uncBaseTax;
    }

    public float getSafeBaseTax() {
        return safeBaseTax;
    }
}
