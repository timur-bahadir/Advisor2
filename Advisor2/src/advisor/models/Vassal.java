package advisor.models;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Timur on 07.04.2015.
 */
public class Vassal implements Comparable {
    private String name;
    private int iTotalCost;
    private int iMonthlyProgress;
    private int iMonthsNeeded;
    private int iStartAt;

    public Vassal(String name, int iTotalCost, int iMonthlyProgress) {
        this.name = name;
        this.iTotalCost = iTotalCost;
        this.iMonthlyProgress = iMonthlyProgress;
    }

    public static ArrayList<Vassal> calculateStarts(ArrayList<Vassal> vassals) {
        for (Vassal v : vassals) {
            v.calcMonthsNeeded();
        }

        Collections.sort(vassals);

        int monthDiff;
        Vassal first = vassals.get(0);
        for (int i = 0; i < vassals.size(); i++) {
            Vassal current = vassals.get(i);

            if (i != 0) {
                monthDiff = first.getMonthsNeeded() - current.getMonthsNeeded();
                current.setStartAt(monthDiff * first.getMonthlyProgress());
            }
        }

        return vassals;
    }

    public void calcMonthsNeeded() {
        iMonthsNeeded = iTotalCost / iMonthlyProgress;
    }

    public int getTotalCost() {
        return iTotalCost;
    }

    public void setTotalCost(int iTotalCost) {
        this.iTotalCost = iTotalCost;
    }

    public int getMonthlyProgress() {
        return iMonthlyProgress;
    }

    public void setMonthlyProgress(int iMonthlyProgress) {
        this.iMonthlyProgress = iMonthlyProgress;
    }

    public int getMonthsNeeded() {
        return iMonthsNeeded;
    }

    public void setMonthsNeeded(int iMonthsNeeded) {
        this.iMonthsNeeded = iMonthsNeeded;
    }

    @Override
    public int compareTo(Object o) {
        Vassal v = (Vassal) o;
        return v.getMonthsNeeded() - iMonthsNeeded;
    }

    public int getStartAt() {
        return iStartAt;
    }

    public void setStartAt(int iStartAt) {
        this.iStartAt = iStartAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
